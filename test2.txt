# Delete car with id 4
curl -X DELETE -i http://localhost:8080/cardealer/cars/4/delete

# Get car with id 4
curl -X GET -i http://localhost:8080/cardealer/cars/4

# Get all cars
curl -X GET -i http://localhost:8080/cardealer/cars

# Add car with id 4 again
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 4, "carBrand" : "Honda", "carModel" : "Fit", "carModelOption" : "LE", "carPrice" : 15000}' http://localhost:8080/cardealer/cars/add

# Get all cars
curl -X GET -i http://localhost:8080/cardealer/cars

# delete car with id 2
curl -X DELETE -i http://localhost:8080/cardealer/cars/2/delete

# Get car with id 2
curl -X GET -i http://localhost:8080/cardealer/cars/2

# Add car with id 2
curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 2, "carBrand" : "BMW", "carModel" : "X3", "carModelOption" : "M3", "carPrice" : 76000}' http://localhost:8080/cardealer/cars/add

# Get all cars
curl -X GET -i http://localhost:8080/cardealer/cars

